package lux.pe.na.model.dto;


import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class TablaMaestra implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String tabla;
    private Integer codigoCampo;
    private String valor;
    private Double valorNumerico;
    private String valorAuxiliar;
    private String equivalenciaVIAP;

}