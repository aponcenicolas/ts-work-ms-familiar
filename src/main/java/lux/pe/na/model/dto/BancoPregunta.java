package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class BancoPregunta implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer idBancoPregunta;
    private Integer codigoTipoDeclaracion;
    private String descripcionPregunta;
    private Integer codigoTipoPregunta;
    private Integer ordenApp;
    private Integer ordenFisicoSolicitud;
    private Integer ordenSubIndiceFisicoSolicitud;
    private Integer aplicaPreguntaMujer;
    private Timestamp fechaCreacion;
    private Timestamp fechaModificacion;
    private Integer estadoSuscripcionInteligente;
    private Integer ordenSuscripcionInteligente;
}
