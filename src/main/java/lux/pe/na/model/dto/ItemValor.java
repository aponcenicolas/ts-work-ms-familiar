package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ItemValor implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer codigo;
    private String descripcion;
}
