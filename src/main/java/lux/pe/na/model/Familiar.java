package lux.pe.na.model;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity(name = "Solicitud.Familiar")
public class Familiar implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdFamiliar")
    private Integer idFamiliar;

    @Column(name = "IdSolicitudMultiple")
    private Integer idSolicitudMultiple;

    @Column(name = "NumeroFamiliarDispositivo")
    private Integer numeroFamiliarDispositivo;

    @Column(name = "CodigoTipoFamiliar")
    private Integer codigoTipoFamiliar;

    @Column(name = "Nombres", length = 40)
    private String nombres;

    @Column(name = "ApellidoPaterno", length = 30)
    private String apellidoPaterno;

    @Column(name = "ApellidoMaterno", length = 30)
    private String apellidoMaterno;

    @Column(name = "CodigoSexo")
    private Integer codigoSexo;

    @Column(name = "CodigoTipoDocumento")
    private Integer codigoTipoDocumento;

    @Column(name = "NumeroDocumento", length = 12)
    private String numeroDocumento;

    @Column(name = "FechaNacimiento")
    private Timestamp fechaNacimiento;

    @Column(name = "CodigoOcupacion")
    private Integer codigoOcupacion;

    @Column(name = "FechaCreacion")
    private Timestamp fechaCreacion;

    @Column(name = "UsuarioCreacion", length = 20)
    private String usuarioCreacion;

    @Column(name = "FechaModificacion")
    private Timestamp fechaModificacion;

    @Column(name = "UsuarioModificacion", length = 20)
    private String usuarioModificacion;



}
