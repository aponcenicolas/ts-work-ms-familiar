package lux.pe.na.model;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity(name = "Solicitud.Respuesta")
public class Respuesta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdRespuesta")
    private Integer idRespuesta;

    @Column(name = "IdSolicitudMultiple")
    private Integer idSolicitudMultiple;

    @Column(name = "IdBancoPregunta")
    private Integer idBancoPregunta;

    @Column(name = "IdAsegurado")
    private Integer idAsegurado;

    @Column(name = "IdContratante")
    private Integer idContratante;

    @Column(name = "IdFamiliar")
    private Integer idFamiliar;

    @Column(name = "FlagRespuesta")
    private Boolean flagRespuesta;

    @Column(name = "FechaCreacion")
    private Timestamp fechaCreacion;

    @Column(name = "UsuarioCreacion",length = 20)
    private String usuarioCreacion;

    @Column(name = "FechaModificacion")
    private Timestamp fechaModificacion;

    @Column(name = "UsuarioModificacion",length = 20)
    private String usuarioModificacion;
}
