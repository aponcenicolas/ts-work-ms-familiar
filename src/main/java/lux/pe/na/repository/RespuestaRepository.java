package lux.pe.na.repository;

import lux.pe.na.model.Respuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RespuestaRepository extends JpaRepository<Respuesta,Integer> {

    List<Respuesta> findAllByIdSolicitudMultiple(Integer idSolicitudMultiple);

}
