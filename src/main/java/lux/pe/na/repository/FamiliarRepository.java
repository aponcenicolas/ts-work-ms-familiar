package lux.pe.na.repository;


import lux.pe.na.model.Familiar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FamiliarRepository extends JpaRepository<Familiar, Integer> {

    List<Familiar> findAllByIdSolicitudMultiple(Integer idSolicitudMultiple);

    Optional<Familiar> findByIdSolicitudMultipleAndIdFamiliar(Integer idSolicitudMultiple, Integer idFamiliar);

    void deleteByIdSolicitudMultipleAndIdFamiliar(Integer idSolicitudMultiple, Integer idFamiliar);
}
