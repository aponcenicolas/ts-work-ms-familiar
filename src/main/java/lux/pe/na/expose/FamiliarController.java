package lux.pe.na.expose;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.business.FamiliarService;
import lux.pe.na.expose.request.FamiliarRequest;
import lux.pe.na.expose.response.Response;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/solicitudes")
@Slf4j
@RequiredArgsConstructor
public class FamiliarController {

    private final FamiliarService familiarService;

    @GetMapping(value = "/{idSolicitudMultiple}/familiares")
    public Mono<Response> obtenerFamiliares(@PathVariable Integer idSolicitudMultiple) {
        return Mono.just(Response.builder().code(200).status("ok").data(familiarService.obtenerFamiliares(idSolicitudMultiple)).build());
    }

    @GetMapping(value = "/{idSolicitudMultiple}/familiares/{idFamiliar}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response> obtenerFamiliar(@PathVariable Integer idSolicitudMultiple, @PathVariable Integer idFamiliar) {
        return Mono.just(Response.builder().code(200).status("ok").data(familiarService.obtenerFamiliar(idSolicitudMultiple, idFamiliar)).build());
    }

    @PostMapping(value = "/{idSolicitudMultiple}/familiares",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response> registrar(@PathVariable Integer idSolicitudMultiple, @RequestBody FamiliarRequest familiarRequest) {
        return Mono.just(Response.builder().code(200).status("ok")
                .data(familiarService.registrarActualizarFamiliar(idSolicitudMultiple, 0, familiarRequest)).build());
    }

    @PutMapping(value = "/{idSolicitudMultiple}/familiares/{idFamiliar}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response> actualizar(@PathVariable Integer idSolicitudMultiple,
                                     @PathVariable Integer idFamiliar,
                                     @RequestBody FamiliarRequest familiarRequest) {
        return Mono.just(Response.builder().code(200).status("ok")
                .data(familiarService.registrarActualizarFamiliar(idSolicitudMultiple, idFamiliar, familiarRequest)).build());
    }

    @DeleteMapping(value = "/{idSolicitudMultiple}/familiares/{idFamiliar}")
    public Mono<Response> eliminar(@PathVariable Integer idSolicitudMultiple, @PathVariable Integer idFamiliar) {
        return Mono.just(Response.builder().code(200).status("ok")
                .data(familiarService.eliminarFamiliar(idSolicitudMultiple, idFamiliar)).build());
    }
}
