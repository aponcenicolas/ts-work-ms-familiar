package lux.pe.na.expose.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class RespuestaRequest {

    private Integer idRespuesta;
    private Integer idBancoPregunta;
    private Integer respuesta;
}
