package lux.pe.na.expose.response;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class RespuestaResponse {

    private Integer idRespuesta;
    private Integer idBancoPregunta;
    private String pregunta;
    private Integer respuesta;
}
