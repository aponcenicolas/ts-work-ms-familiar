package lux.pe.na.expose.response;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ExceptionResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;
    private String status;
    private Object data;
    private String message;
}
