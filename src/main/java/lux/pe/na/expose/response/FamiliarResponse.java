package lux.pe.na.expose.response;


import lombok.*;
import lux.pe.na.model.dto.ItemValor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class FamiliarResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idFamiliar;
    private Integer numeroFamiliar;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private ItemValor parentesco;
    private ItemValor tipoDocumento;
    private String numeroDocumento;
    private ItemValor ocupacion;
    private ItemValor sexo;
    private Timestamp fechaNacimiento;
    private List<RespuestaResponse> respuestas;
}
