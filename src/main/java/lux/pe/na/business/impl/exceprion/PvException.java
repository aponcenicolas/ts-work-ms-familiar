package lux.pe.na.business.impl.exceprion;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PvException extends RuntimeException {
    private static final Long serialVersionUID = 1L;

    private final String mensaje;

}
