package lux.pe.na.business.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.business.FamiliarService;
import lux.pe.na.business.impl.exceprion.PvException;
import lux.pe.na.expose.request.FamiliarRequest;
import lux.pe.na.expose.response.FamiliarResponse;
import lux.pe.na.model.Familiar;
import lux.pe.na.model.Respuesta;
import lux.pe.na.model.dto.BancoPregunta;
import lux.pe.na.model.dto.ItemValor;
import lux.pe.na.model.dto.TablaMaestra;
import lux.pe.na.repository.FamiliarRepository;
import lux.pe.na.repository.RespuestaRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class FamiliarServiceImpl implements FamiliarService {

    private final FamiliarRepository familiarRepository;
    private final RespuestaRepository respuestaRepository;

    @Override
    public Mono<List<FamiliarResponse>> obtenerFamiliares(Integer idSolicitudMultiple) {
        List<BancoPregunta> bancoPreguntas = null;
        List<TablaMaestra> tablaMaestras = null;
        List<Familiar> familiares;
        List<Respuesta> respuestas;
        List<FamiliarResponse> familiarRespuestas = new ArrayList<>();
        FamiliarResponse familiarResponse;

        familiares = familiarRepository.findAllByIdSolicitudMultiple(idSolicitudMultiple);

        List<TablaMaestra> tipoOcupacion = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 9).collect(Collectors.toList());
        List<TablaMaestra> tipoDocId = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 13).collect(Collectors.toList());
        List<TablaMaestra> tipoSexo = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 15).collect(Collectors.toList());
        List<TablaMaestra> tipoParentesco = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 36).collect(Collectors.toList());

        respuestas = respuestaRepository.findAllByIdSolicitudMultiple(idSolicitudMultiple);

        if (bancoPreguntas == null) {
            bancoPreguntas = new ArrayList<>();
        }

        if (respuestas == null) {
            respuestas = new ArrayList<>();
        }

        for (Familiar familiar : familiares) {
            familiarResponse = transformarFamiliarResponse(familiar, tipoOcupacion, tipoDocId, tipoSexo, tipoParentesco, bancoPreguntas, respuestas);
            familiarRespuestas.add(familiarResponse);
        }

        return Mono.just(familiarRespuestas);
    }

    @Override
    public Mono<FamiliarResponse> obtenerFamiliar(Integer idSolicitudMultiple, Integer idFamiliar) {
        Familiar familiar;
        List<BancoPregunta> bancoPreguntas = null;
        List<TablaMaestra> tablaMaestras = null;
        List<TablaMaestra> tipoOcupacion;
        List<TablaMaestra> tipoDocId;
        List<TablaMaestra> tipoSexo;
        List<TablaMaestra> tipoParentesco;

        familiar = familiarRepository.findByIdSolicitudMultipleAndIdFamiliar(idSolicitudMultiple, idFamiliar)
                .orElseThrow(() -> new PvException("No se encontro el familiar con el id " + idFamiliar));

        if (familiar == null) {
            log.info("No existe Familiar registrado con el codigo {}", idFamiliar);
            throw new PvException("No existe Familiar registrado con el id " + idFamiliar);
        } else {
            if (!Objects.equals(familiar.getIdSolicitudMultiple(), idSolicitudMultiple)) {
                log.info("El familiar con el codigo {}  no corresponde a la solicitud multiple {}", idFamiliar, idSolicitudMultiple);
                throw new PvException("El Familiar con el id " + idFamiliar + " no corresponde a la solicitud multiple " + idSolicitudMultiple);
            }
        }

        tipoOcupacion = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 9).collect(Collectors.toList());
        tipoDocId = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 13).collect(Collectors.toList());
        tipoSexo = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 15).collect(Collectors.toList());
        tipoParentesco = tablaMaestras.stream().filter(tablaMaestra -> tablaMaestra.getId() == 36).collect(Collectors.toList());

        List<Respuesta> respuestas = null;
        if (idFamiliar != null) {
            respuestas = respuestaRepository.findAllByIdSolicitudMultiple(idSolicitudMultiple);
        }

        if (bancoPreguntas == null) {
            bancoPreguntas = new ArrayList<>();
        }

        if (respuestas == null) {
            respuestas = new ArrayList<>();
        }
        return Mono.just(transformarFamiliarResponse(familiar, tipoOcupacion, tipoDocId, tipoSexo, tipoParentesco, bancoPreguntas, respuestas));
    }

    @Override
    public Mono<FamiliarResponse> registrarActualizarFamiliar(Integer idSolicitudMultiple, Integer idFamiliar, FamiliarRequest familiarRequest) {
        return null;
    }

    @Override
    public Mono<String> eliminarFamiliar(Integer idSolicitudMultiple, Integer idFamiliar) {
        Optional<Familiar> familiar = familiarRepository.findByIdSolicitudMultipleAndIdFamiliar(idSolicitudMultiple, idFamiliar);

        String message;
        if (familiar.isPresent()) {
            familiarRepository.deleteByIdSolicitudMultipleAndIdFamiliar(idSolicitudMultiple, idFamiliar);
            message = "Eliminado correctamente";
        } else {
            throw new PvException("Error al eliminar familiar");
        }
        return Mono.just(message);
    }

    private FamiliarResponse transformarFamiliarResponse(
            Familiar familiar,
            List<TablaMaestra> tipoOcupacion,
            List<TablaMaestra> tipoDocId,
            List<TablaMaestra> tipoSexo,
            List<TablaMaestra> tipoParentesco,
            List<BancoPregunta> bancoPreguntas,
            List<Respuesta> respuestas
    ) {

        FamiliarResponse familiarResponse = FamiliarResponse.builder()
                .parentesco(new ItemValor())
                .tipoDocumento(new ItemValor())
                .idFamiliar(familiar.getIdFamiliar())
                .numeroFamiliar(familiar.getNumeroFamiliarDispositivo())
                .apellidoPaterno(familiar.getApellidoPaterno())
                .apellidoMaterno(familiar.getApellidoMaterno())
                .nombres(familiar.getNombres())
                .fechaNacimiento(familiar.getFechaNacimiento())
                .numeroDocumento(familiar.getNumeroDocumento())
                .build();

        if (familiar.getCodigoOcupacion() != 0) {
            familiarResponse.setOcupacion(ItemValor.builder()
                    .codigo(familiar.getCodigoOcupacion())
                    .descripcion(tipoOcupacion.stream()
                            .filter(tablaMaestra -> tablaMaestra.getCodigoCampo()
                                    .equals(familiar.getCodigoOcupacion()))
                            .map(TablaMaestra::getValor)
                            .findFirst()
                            .orElse(""))
                    .build());
        }

        familiarResponse.getParentesco().setCodigo(familiar.getCodigoTipoFamiliar());
        familiarResponse.getParentesco().setDescripcion(tipoParentesco.stream()
                .filter(tablaMaestra -> tablaMaestra.getCodigoCampo()
                        .equals(familiar.getCodigoTipoFamiliar()))
                .map(TablaMaestra::getValor)
                .findFirst()
                .orElse(""));

        if (familiar.getCodigoSexo() != 2) {
            familiarResponse.setSexo(ItemValor.builder()
                    .codigo(familiar.getCodigoSexo())
                    .descripcion(tipoSexo.stream()
                            .filter(tablaMaestra -> tablaMaestra.getCodigoCampo()
                                    .equals(familiar.getCodigoSexo()))
                            .map(TablaMaestra::getValor)
                            .findFirst()
                            .orElse(""))
                    .build());
        }

        familiarResponse.getTipoDocumento().setCodigo(familiar.getCodigoTipoDocumento());
        familiarResponse.getTipoDocumento().setDescripcion(tipoDocId.stream()
                .filter(tablaMaestra -> tablaMaestra.getCodigoCampo()
                        .equals(familiar.getCodigoTipoFamiliar()))
                .map(TablaMaestra::getValor)
                .findFirst()
                .orElse(""));

        return familiarResponse;
    }

}
