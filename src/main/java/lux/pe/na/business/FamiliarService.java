package lux.pe.na.business;

import lux.pe.na.expose.request.FamiliarRequest;
import lux.pe.na.expose.response.FamiliarResponse;
import reactor.core.publisher.Mono;

import java.util.List;

public interface FamiliarService {

    Mono<List<FamiliarResponse>> obtenerFamiliares(Integer idSolicitudMultiple);

    Mono<FamiliarResponse> obtenerFamiliar(Integer idSolicitudMultiple, Integer idFamiliar);

    Mono<FamiliarResponse> registrarActualizarFamiliar(Integer idSolicitudMultiple, Integer idFamiliar, FamiliarRequest familiarRequest);

    Mono<String> eliminarFamiliar(Integer idSolicitudMultiple, Integer idFamiliar);
}
